# Edifactotum

Parse and validate UN EDIFACT messages.

This is the MVP of a UN EDIFACT message processor.

# License

Published under the MIT-License, see [LICENSE-MIT.txt](./LICENSE-MIT.txt) file.

# Contact

Your feedback is appreciated, please e-mail me at [alaarmann@gmx.net](mailto:alaarmann@gmx.net)
