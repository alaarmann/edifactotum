use serde::Serialize;

#[derive(Copy, Clone, Debug, PartialEq, Serialize)]
pub enum InclusionStatus {
    Mandatory,
    Conditional,
}

impl From<&str> for InclusionStatus {
    fn from(input: &str) -> Self {
        match input {
            "M" => InclusionStatus::Mandatory,
            "C" => InclusionStatus::Conditional,
            _ => unreachable!(),
        }
    }
}
