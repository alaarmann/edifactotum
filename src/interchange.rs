use pest::iterators::Pair;
use pest::{Parser, Span};
use std::error::Error;

#[derive(Parser)]
#[grammar = "interchange.pest"]
pub struct EdifactParser;

#[derive(Debug)]
pub struct Interchange<'a> {
    pub messages: Vec<Message<'a>>,
}

#[derive(Debug)]
pub struct Message<'a> {
    reference_number: Component,
    pub identifier: Identifier,
    pub segments: Vec<Segment<'a>>,
}

impl<'a> From<Pair<'a, Rule>> for Message<'a> {
    fn from(pair: Pair<'a, Rule>) -> Self {
        let mut inner = pair.into_inner();
        let unh = inner.nth(0).unwrap();
        println!("unh: {:?}", unh);
        assert!(unh.as_rule() == Rule::unh);
        let mut unh_inner = unh.into_inner();
        let reference_number =
            Component::from(unh_inner.nth(0).unwrap().into_inner().nth(0).unwrap());
        let identifier = Identifier::from(unh_inner.nth(0).unwrap());
        let segments = inner
            .filter(|s| s.as_rule() == Rule::segment)
            .map(Segment::from)
            .collect();

        Message {
            reference_number,
            identifier,
            segments,
        }
    }
}

#[derive(Debug)]
pub struct Identifier {
    pub message_type: Component,
    pub version_number: Component,
    pub release_number: Component,
    pub controlling_agency: Component,
}

impl<'a> From<Pair<'a, Rule>> for Identifier {
    fn from(pair: Pair<'a, Rule>) -> Self {
        let mut inner = pair.into_inner();
        let message_type = Component::from(inner.nth(0).unwrap());
        let version_number = Component::from(inner.nth(0).unwrap());
        let release_number = Component::from(inner.nth(0).unwrap());
        let controlling_agency = Component::from(inner.nth(0).unwrap());

        Identifier {
            message_type,
            version_number,
            release_number,
            controlling_agency,
        }
    }
}

#[derive(Debug)]
pub struct Segment<'a> {
    pub tag: Tag<'a>,
    pub elements: Vec<Element<'a>>,
    pub span: Span<'a>,
}

impl<'a> From<Pair<'a, Rule>> for Segment<'a> {
    fn from(pair: Pair<'a, Rule>) -> Self {
        let nest_repeat_indication: Vec<usize> = vec![];
        let span = pair.as_span();
        let mut inner = pair.into_inner();
        let code = inner.nth(0).map(|c| c.as_str()).unwrap();
        let elements = inner.map(Element::from).collect();

        Segment {
            tag: Tag {
                code,
                nest_repeat_indication,
            },
            elements,
            span,
        }
    }
}

#[derive(Debug)]
pub struct Tag<'a> {
    pub code: &'a str,
    pub nest_repeat_indication: Vec<usize>,
}

#[derive(Debug)]
pub struct Element<'a> {
    pub components: Vec<Component>,
    span: Span<'a>,
}

impl<'a> From<Pair<'a, Rule>> for Element<'a> {
    fn from(pair: Pair<'a, Rule>) -> Self {
        let span = pair.as_span();
        let inner = pair.into_inner();

        let components = inner
            .rev()
            .skip_while(|p| p.as_span().as_str().is_empty())
            .collect::<Vec<_>>()
            .into_iter()
            .rev()
            .map(Component::from)
            .collect();

        Element { components, span }
    }
}

#[derive(Debug)]
pub struct Component(pub String);

impl<'a> From<Pair<'a, Rule>> for Component {
    fn from(pair: Pair<Rule>) -> Self {
        Component(
            pair.into_inner()
                .map(|p| p.as_str())
                .fold(String::from(""), |acc, s| acc + s),
        )
    }
}

pub fn parse(input: &str) -> Result<Interchange, Box<dyn Error>> {
    let parsed = EdifactParser::parse(Rule::interchange, input)?
        .next()
        .unwrap();
    let messages = parsed
        .into_inner()
        .filter_map(|segment| match segment.as_rule() {
            Rule::message => {
                let message = Message::from(segment);
                println!("message: {:?}", message);
                Some(message)
            }
            _ => None,
        })
        .collect();
    Ok(Interchange { messages })
}

#[cfg(test)]
mod tests {
    use std::fs;
    use test_case::test_case;

    use super::*;

    #[test_case( "data/interchange/simple.edi" ; "simple")]
    #[test_case( "data/interchange/no_header.edi" ; "no header")]
    #[test_case( "data/interchange/release_char.edi" ; "release char")]
    #[test_case( "data/interchange/custom_separator.edi" ; "custom separator")]
    #[test_case( "data/interchange/functional_group.edi" ; "functional group")]
    #[test_case( "data/interchange/example_01.edi" ; "example 1")]
    #[test_case( "data/interchange/example_02.edi" ; "example 2")]
    fn parse_interchange(filename: &str) {
        let input = fs::read_to_string(filename)
            .expect(format!("Error reading file {}", filename).as_str());

        let result = parse(input.as_str());

        assert!(result.is_ok())
    }

    #[test]
    fn parse_empty_component() {
        let input = "EM2:0:1630::6";
        let parsed = EdifactParser::parse(Rule::composite, input)
            .unwrap()
            .next()
            .unwrap();

        let element = Element::from(parsed);
        let empty_component = element.components.iter().nth(3).unwrap();
        assert!(empty_component.0.is_empty());
    }

    #[test]
    fn parse_empty_element() {
        let input = "APD+EM2:0:1630::6+++++++DA\'";
        let parsed = EdifactParser::parse(Rule::segment, input)
            .unwrap()
            .next()
            .unwrap();

        let segment = Segment::from(parsed);
        let empty_element = segment.elements.iter().nth(1).unwrap();
        assert!(empty_element.components.is_empty());
    }
}
