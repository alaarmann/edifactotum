use crate::common::InclusionStatus;
use codepage_437::{BorrowFromCp437, CP437_CONTROL};
use pest::iterators::{Pair, Pairs};
use pest::Parser;
use serde::Serialize;
use std::error::Error;
use std::fs::read;

#[derive(Parser)]
#[grammar = "segment_definition.pest"]
pub struct SegmentDefinitionParser;

#[derive(Debug, PartialEq, Serialize)]
pub struct SegmentDefinition<'a> {
    pub tag: &'a str,
    pub name: &'a str,
    pub description: String,
    pub elements: Vec<ElementDefinitionObject<'a>>,
    pub note: Option<String>,
}

impl<'a> From<Pair<'a, Rule>> for SegmentDefinition<'a> {
    fn from(pair: Pair<'a, Rule>) -> Self {
        let mut inner = pair.into_inner();

        let tag = get_string_of_rule(&mut inner, Rule::tag);
        let name = get_string_of_rule(&mut inner, Rule::name).trim_end();
        let description = inner
            .next()
            .unwrap()
            .into_inner()
            .filter(|p| p.as_rule() == Rule::description_line)
            .map(|r| r.as_str().trim_start().trim_end())
            .fold(String::new(), join_with_space);

        let elements = inner
            .next()
            .unwrap()
            .into_inner()
            .filter(|p| p.as_rule() == Rule::element || p.as_rule() == Rule::composite_element)
            .map(ElementDefinitionObject::from)
            .collect();

        let note = inner.next().map(|p| {
            p.into_inner()
                .filter(|p| p.as_rule() == Rule::note_line)
                .map(|r| r.as_str().trim_start().trim_end())
                .fold(String::new(), join_with_space)
        });

        SegmentDefinition {
            tag,
            name,
            description,
            elements,
            note,
        }
    }
}

#[derive(Debug, PartialEq, Serialize)]
pub enum ElementDefinitionObject<'a> {
    ElementDefinition(ElementDefinition<'a>),
    CompositeElementDefinition(CompositeElementDefinition<'a>),
}

impl<'a> From<Pair<'a, Rule>> for ElementDefinitionObject<'a> {
    fn from(pair: Pair<'a, Rule>) -> Self {
        match pair.as_rule() {
            Rule::element => {
                ElementDefinitionObject::ElementDefinition(ElementDefinition::from(pair))
            }
            Rule::composite_element => ElementDefinitionObject::CompositeElementDefinition(
                CompositeElementDefinition::from(pair),
            ),
            _ => unreachable!(),
        }
    }
}

#[derive(Debug, PartialEq, Serialize)]
pub struct ElementDefinition<'a> {
    pub position: &'a str,
    pub tag: &'a str,
    pub name: String,
    pub status: InclusionStatus,
    pub repetition: usize,
    pub data_type: &'a str,
}

impl<'a> From<Pair<'a, Rule>> for ElementDefinition<'a> {
    fn from(pair: Pair<'a, Rule>) -> Self {
        let mut inner = pair.into_inner();

        let position = get_string_of_rule(&mut inner, Rule::position);
        let tag = get_string_of_rule(&mut inner, Rule::element_tag);
        let name = inner
            .next()
            .unwrap()
            .into_inner()
            .filter(|p| p.as_rule() == Rule::element_name_part)
            .map(|r| r.as_str().trim_start().trim_end())
            .fold(String::new(), join_with_space);

        let status = InclusionStatus::from(get_string_of_rule(&mut inner, Rule::status));
        let repetition = get_string_of_rule(&mut inner, Rule::repetition)
            .parse()
            .unwrap();
        let data_type = get_string_of_rule(&mut inner, Rule::data_type);

        ElementDefinition {
            position,
            tag,
            name,
            status,
            repetition,
            data_type,
        }
    }
}

#[derive(Debug, PartialEq, Serialize)]
pub struct CompositeElementDefinition<'a> {
    pub position: &'a str,
    pub tag: &'a str,
    pub name: String,
    pub status: InclusionStatus,
    pub repetition: usize,
    pub components: Vec<ComponentDefinition<'a>>,
}

impl<'a> From<Pair<'a, Rule>> for CompositeElementDefinition<'a> {
    fn from(pair: Pair<'a, Rule>) -> Self {
        let mut inner = pair.into_inner();

        let position = get_string_of_rule(&mut inner, Rule::position);
        let tag = get_string_of_rule(&mut inner, Rule::element_tag);
        let name = inner
            .next()
            .unwrap()
            .into_inner()
            .filter(|p| p.as_rule() == Rule::element_name_part)
            .map(|r| r.as_str().trim_start().trim_end())
            .fold(String::new(), join_with_space);

        let status = InclusionStatus::from(get_string_of_rule(&mut inner, Rule::status));
        let repetition = get_string_of_rule(&mut inner, Rule::repetition)
            .parse()
            .unwrap();
        let components = inner
            .filter(|p| p.as_rule() == Rule::component)
            .map(ComponentDefinition::from)
            .collect();

        CompositeElementDefinition {
            position,
            tag,
            name,
            status,
            repetition,
            components,
        }
    }
}

#[derive(Debug, PartialEq, Serialize)]
pub struct ComponentDefinition<'a> {
    pub tag: &'a str,
    pub description: String,
    pub status: InclusionStatus,
    pub data_type: &'a str,
}

impl<'a> From<Pair<'a, Rule>> for ComponentDefinition<'a> {
    fn from(pair: Pair<'a, Rule>) -> Self {
        let mut inner = pair.into_inner();

        let tag = get_string_of_rule(&mut inner, Rule::component_tag);
        let description = inner
            .next()
            .unwrap()
            .into_inner()
            .filter(|p| p.as_rule() == Rule::component_description_part)
            .map(|r| r.as_str().trim_start().trim_end())
            .fold(String::new(), join_with_space);

        let status = InclusionStatus::from(get_string_of_rule(&mut inner, Rule::status));
        let data_type = get_string_of_rule(&mut inner, Rule::data_type);

        ComponentDefinition {
            tag,
            description,
            status,
            data_type,
        }
    }
}

fn get_string_of_rule<'a>(pairs: &mut Pairs<'a, Rule>, rule: Rule) -> &'a str {
    pairs
        .next()
        .filter(|p| p.as_rule() == rule)
        .unwrap()
        .as_str()
}

fn join_with_space<'a>(acc: String, part: &str) -> String {
    match acc.len() {
        0 => acc + part,
        _ => acc + " " + part,
    }
}

pub fn parse(input: &str) -> Result<Vec<SegmentDefinition>, Box<dyn Error>> {
    let parsed = SegmentDefinitionParser::parse(Rule::segment_definitions, input)?
        .next()
        .unwrap();

    let inner = parsed.into_inner();
    let segment_definitions = inner
        .filter(|p| p.as_rule() == Rule::segment)
        .map(SegmentDefinition::from)
        .collect::<Vec<_>>();

    Ok(segment_definitions)
}

pub fn to_json(segment_definition: &SegmentDefinition) -> String {
    serde_json::to_string(segment_definition).unwrap()
}

pub fn read_file(filename: &str) -> Result<String, Box<dyn Error>> {
    let buffer = read(filename)?;
    Ok(String::borrow_from_cp437(&buffer, &CP437_CONTROL))
}

#[cfg(test)]
mod tests {
    use test_case::test_case;

    use super::*;

    #[test_case( "data/specification/segment/EXAMPLE.20A" ; "new format")]
    #[test_case( "data/specification/segment/EXAMPLE.99B" ; "old format")]
    #[ignore = "needs un directory, respect license"]
    fn parse_segment_definition_file(filename: &str) {
        let input = read_file(filename).expect(format!("Error reading file {}", filename).as_str());

        let result = parse(input.as_str());

        println!("{:?}", result);
        assert!(result.is_ok())
    }

    #[test_case( "----------------------------------------------------------------------

       ABC  SEGMENT ABC

       Description text here.

010    1234 ANY NAME                                   M    1 an..35
", SegmentDefinition {
    tag: "ABC",
    name: "SEGMENT ABC",
    description: String::from("Description text here."),
    elements: vec![
      ElementDefinitionObject::ElementDefinition(
          ElementDefinition {
            position: "010",
            tag: "1234",
            name: String::from("ANY NAME"),
            status: InclusionStatus::Mandatory,
            repetition: 1,
            data_type: "an..35"
          }
      )
    ],
    note: None
} ; "simple")]
    #[test_case( "----------------------------------------------------------------------

       ABC  SEGMENT ABC

       Function: Description text here that is running over some
                 lines and is running again over the very next
                 line.

010    1234 ANY NAME                                   M    1 an..35
", SegmentDefinition {
    tag: "ABC",
    name: "SEGMENT ABC",
    description: String::from("Function: Description text here that is running over some lines and is running again over the very next line."),
    elements: vec![
      ElementDefinitionObject::ElementDefinition(
          ElementDefinition {
            position: "010",
            tag: "1234",
            name: String::from("ANY NAME"),
            status: InclusionStatus::Mandatory,
            repetition: 1,
            data_type: "an..35"
          }
      )
    ],
    note: None
} ; "multi line description")]
    #[test_case( "----------------------------------------------------------------------

       ABC  SEGMENT ABC

       Description text here.

010    1234 ANY NAME                                   M    1 an..35

       Note: 
            First sentence of description running over more
            than one line.
            Second sentence of description running over more
            than one line.

", SegmentDefinition {
    tag: "ABC",
    name: "SEGMENT ABC",
    description: String::from("Description text here."),
    elements: vec![
      ElementDefinitionObject::ElementDefinition(
          ElementDefinition {
            position: "010",
            tag: "1234",
            name: String::from("ANY NAME"),
            status: InclusionStatus::Mandatory,
            repetition: 1,
            data_type: "an..35"
          }
      )
    ],
    note: Some("Note: First sentence of description running over more than one line. Second sentence of description running over more than one line.".to_string())
} ; "with note")]
    fn parse_segment_definition(input: &str, expected: SegmentDefinition) {
        let parsed = SegmentDefinitionParser::parse(Rule::segment, input)
            .unwrap()
            .next()
            .unwrap();

        let result = SegmentDefinition::from(parsed);

        assert_eq!(result, expected)
    }

    #[test_case( "040    1234 ANY NAME                                   C    3 an..35\n",
    ElementDefinition {
      position: "040",
      tag: "1234",
      name: String::from("ANY NAME"),
      status: InclusionStatus::Conditional,
      repetition: 3,
      data_type: "an..35"
    } ; "simple")]
    #[test_case( "010    1234 NAME RUNNING OVER FIRST LINE WITH
            CONTINUATION                               M    1 an..3
",
    ElementDefinition {
      position: "010",
      tag: "1234",
      name: String::from("NAME RUNNING OVER FIRST LINE WITH CONTINUATION"),
      status: InclusionStatus::Mandatory,
      repetition: 1,
      data_type: "an..3"
    } ; "multi line name")]
    fn parse_element_definition(input: &str, expected: ElementDefinition) {
        let parsed = SegmentDefinitionParser::parse(Rule::element, input)
            .unwrap()
            .next()
            .unwrap();

        let result = ElementDefinition::from(parsed);

        assert_eq!(result, expected)
    }

    #[test_case( "       1235  Component description running over linexx
             continued                                 M      an..3
",
    ComponentDefinition {
        tag: "1235",
        description: "Component description running over linexx continued".to_string(),
        status: InclusionStatus::Mandatory,
        data_type: "an..3"
    } ; "multi-line component description")]

    fn parse_component_definition(input: &str, expected: ComponentDefinition) {
        let parsed = SegmentDefinitionParser::parse(Rule::component, input)
            .unwrap()
            .next()
            .unwrap();

        let result = ComponentDefinition::from(parsed);

        assert_eq!(result, expected)
    }

    #[test_case( "050    A123 NAME OF COMPOSITE                          C    1
       2345  Name of first component                   M      an..3
       3456  Name of second component                  C      an..4
       4567  Name of third component                   C      an..5

",
    CompositeElementDefinition {
      position: "050",
      tag: "A123",
      name: String::from("NAME OF COMPOSITE"),
      status: InclusionStatus::Conditional,
      repetition: 1,
      components: vec![
          ComponentDefinition {
              tag: "2345",
              description: "Name of first component".to_string(),
              status: InclusionStatus::Mandatory,
              data_type: "an..3"
          },
          ComponentDefinition {
              tag: "3456",
              description: "Name of second component".to_string(),
              status: InclusionStatus::Conditional,
              data_type: "an..4"
          },
          ComponentDefinition {
              tag: "4567",
              description: "Name of third component".to_string(),
              status: InclusionStatus::Conditional,
              data_type: "an..5"
          }
      ]
    } ; "composite")]
    #[test_case( "060    A123 NAME OF COMPOSITE                          C    8
       1235  Component name running over in next
             description line                          C      an..17

",
    CompositeElementDefinition {
      position: "060",
      tag: "A123",
      name: String::from("NAME OF COMPOSITE"),
      status: InclusionStatus::Conditional,
      repetition: 8,
      components: vec![
          ComponentDefinition {
              tag: "1235",
              description: "Component name running over in next description line".to_string(),
              status: InclusionStatus::Conditional,
              data_type: "an..17"
          },
      ],
    } ; "multi-line description")]

    fn parse_composite_element_definition(input: &str, expected: CompositeElementDefinition) {
        let parsed = SegmentDefinitionParser::parse(Rule::composite_element, input)
            .unwrap()
            .next()
            .unwrap();

        let result = CompositeElementDefinition::from(parsed);

        assert_eq!(result, expected)
    }
}
