#[macro_use]
extern crate pest_derive;
mod common;
mod definition;
mod interchange;
mod message_definition;
mod segment_definition;

use crate::common::InclusionStatus;
use crate::definition::{
    get_definition_for, DefinitionObject, ElementDefinitionObject, MessageDefinition,
    MessageIdentifier, SegmentGroupDefinition,
};
use crate::interchange::{
    parse as parse_raw_interchange, Element as RawElement, Interchange, Message,
    Segment as RawSegment,
};
use std::error::Error;
use std::iter::Peekable;
use std::path::PathBuf;

#[derive(Debug)]
pub struct ValidatedInterchange {
    pub messages: Vec<ValidatedMessage>,
}

#[derive(Debug)]
pub struct ValidatedMessage {
    objects: Vec<SegmentObject>,
}

#[derive(Debug)]
pub struct Segment {
    tag: String,
    name: String,
    general_name: String,
    description: String,
    values: Vec<ElementObject>,
    note: Option<String>,
}

#[derive(Debug)]
pub enum ElementObject {
    Element(Element),
    CompositeElement(CompositeElement),
}

#[derive(Debug)]
pub struct Element {
    tag: String,
    name: String,
    value: String,
}

#[derive(Debug)]
pub struct CompositeElement {
    tag: String,
    name: String,
    components: Vec<ElementComponent>,
}

#[derive(Debug)]
pub struct ElementComponent {
    tag: String,
    description: String,
    value: String,
}

#[derive(Debug)]
pub struct SegmentGroup {
    name: String,
    objects: Vec<Vec<SegmentObject>>,
}

#[derive(Debug)]
pub enum SegmentObject {
    Segment(Segment),
    SegmentGroup(SegmentGroup),
}

#[derive(Debug)]
pub enum Repetition<T> {
    One(T),
    Many(Vec<T>),
}

pub fn read_interchange(input: &str) -> Result<ValidatedInterchange, Box<dyn Error>> {
    let raw_interchange = parse_raw_interchange(input)?;

    let validated_messages = raw_interchange
        .messages
        .into_iter()
        .map(to_validated_message)
        .collect::<Result<Vec<ValidatedMessage>, Box<dyn Error>>>()?;

    Ok(ValidatedInterchange {
        messages: validated_messages,
    })
}

fn to_validated_message(raw_message: Message) -> Result<ValidatedMessage, Box<dyn Error>> {
    let message_identifier = get_identifier_from(&raw_message);
    let message_definition = get_definition_for(
        PathBuf::from("data/specification").into_boxed_path(),
        &message_identifier,
    )?;
    validate_message(&raw_message, &message_definition)
}

fn get_identifier_from<'a>(message: &'a Message) -> MessageIdentifier<'a> {
    let message_type = &message.identifier.message_type.0;
    let controlling_agency = &message.identifier.controlling_agency.0;
    let release_number = &message.identifier.release_number.0;
    let version_number = &message.identifier.version_number.0;

    MessageIdentifier {
        message_type: message_type.as_str(),
        controlling_agency: controlling_agency.as_str(),
        release_number: release_number.as_str(),
        version_number: version_number.as_str(),
    }
}

fn validate_message(
    message: &Message,
    message_definition: &MessageDefinition,
) -> Result<ValidatedMessage, Box<dyn Error>> {
    let mut segments = message.segments.iter().peekable();
    let objects = message_definition
        .objects
        .iter()
        .map(|definition_object| parse_segment_object(definition_object, &mut segments))
        .collect::<Result<Vec<_>, Box<dyn Error>>>()?
        .into_iter()
        .flatten()
        .collect();

    if let Some(leftover_segment) = segments.peek() {
        let err: Box<dyn Error + Send + Sync> = From::from(format!(
            "Unexpected segment {} after parsing",
            leftover_segment.tag.code
        ));
        Err(err as Box<dyn Error>)
    } else {
        Ok(ValidatedMessage { objects })
    }
}

fn parse_segment_object<'a, I: Iterator<Item = &'a RawSegment<'a>> + 'a>(
    definition_object: &DefinitionObject,
    segments: &mut Peekable<I>,
) -> Result<Vec<SegmentObject>, Box<dyn Error>> {
    // TODO collect by iterator
    let mut result = vec![];

    match &definition_object {
        DefinitionObject::Segment(segment_definition) => {
            while let Some(next_segment) = segments.peek() {
                if next_segment.tag.code == segment_definition.tag
                    && result.len() < segment_definition.repetition
                {
                    let values: Vec<ElementObject> = parse_element_objects(
                        segment_definition.elements.as_slice(),
                        next_segment.elements.as_slice(),
                    )?;
                    result.push(SegmentObject::Segment(Segment {
                        tag: next_segment.tag.code.to_string(),
                        name: segment_definition.name.to_string(),
                        general_name: segment_definition.general_name.to_string(),
                        description: segment_definition.description.to_string(),
                        values,
                        note: segment_definition.note.clone(),
                    }));
                    segments.next();
                } else {
                    break;
                }
            }
            if result.is_empty() && segment_definition.status == InclusionStatus::Mandatory {
                let err: Box<dyn Error + Send + Sync> = From::from(format!(
                    "Mandatory segment {} missing",
                    segment_definition.tag
                ));
                Err(err as Box<dyn Error>)
            } else if result.len() > segment_definition.repetition {
                let err: Box<dyn Error + Send + Sync> = From::from(format!(
                    "Too many repetitions ({}) for segment {}, max. allowed: {}",
                    result.len(),
                    segment_definition.tag,
                    segment_definition.repetition
                ));
                Err(err as Box<dyn Error>)
            } else {
                Ok(result)
            }
        }
        DefinitionObject::SegmentGroup(segment_group_definition) => {
            let mut objects = vec![];
            loop {
                let parse_result = parse_segment_group_objects(segment_group_definition, segments);

                let result = match parse_result {
                    Ok(_) => parse_result,
                    Err(_) => match segment_group_definition.status {
                        InclusionStatus::Mandatory => {
                            if objects.is_empty() {
                                parse_result
                            } else {
                                Ok(vec![])
                            }
                        }
                        InclusionStatus::Conditional => Ok(vec![]),
                    },
                };
                match result {
                    Ok(objects_one_repetition) => {
                        let is_last_repetition_empty = objects_one_repetition.is_empty();
                        if !is_last_repetition_empty {
                            objects.push(objects_one_repetition);
                        }
                        if is_last_repetition_empty
                            || objects.len() == segment_group_definition.repetition
                        {
                            let overall_result = if !objects.is_empty() {
                                vec![SegmentObject::SegmentGroup(SegmentGroup {
                                    name: segment_group_definition.name.to_string(),
                                    objects,
                                })]
                            } else {
                                vec![]
                            };
                            break Ok(overall_result);
                        }
                    }
                    Err(_) => {
                        break result;
                    }
                }
            }
        }
    }
}

fn parse_segment_group_objects<'a, I: Iterator<Item = &'a RawSegment<'a>> + 'a>(
    segment_group_definition: &SegmentGroupDefinition,
    segments: &mut Peekable<I>,
) -> Result<Vec<SegmentObject>, Box<dyn Error>> {
    let result = segment_group_definition
        .objects
        .iter()
        .map(|definition_object| parse_segment_object(&definition_object, segments))
        .collect::<Result<Vec<Vec<SegmentObject>>, Box<dyn Error>>>()?
        .into_iter()
        .flatten()
        .collect();
    Ok(result)
}

fn parse_element_objects(
    definition_elements: &[ElementDefinitionObject],
    segment_elements: &[RawElement],
) -> Result<Vec<ElementObject>, Box<dyn Error>> {
    let parsed: Result<Vec<ElementObject>, Box<dyn Error>> = definition_elements
        .iter()
        .flat_map(|definition| {
            let repetition = match definition {
                ElementDefinitionObject::ElementDefinition(element_definition) => {
                    (0..element_definition.repetition).into_iter()
                }
                ElementDefinitionObject::CompositeElementDefinition(element_definition) => {
                    (0..element_definition.repetition).into_iter()
                }
            };
            repetition.map(move |_| definition)
        })
        .zip(segment_elements.iter())
        .filter_map(|(definition, element)| {
            let repetition = match definition {
                ElementDefinitionObject::ElementDefinition(element_definition) => {
                    let len = element.components.len();
                    if len > 1 {
                        let err: Box<dyn Error + Send + Sync> = From::from(format!(
                            "Expected single component element for element tag {}, found {} components instead", element_definition.tag,
                            len
                        ));
                        Some(Err(err as Box<dyn Error>))
                    } else {
                        element.components.get(0).map(|comp| Ok(ElementObject::Element(Element {
                            tag: element_definition.tag.to_string(),
                            name: element_definition.name.to_string(),
                            value: comp.0.to_string(),
                        })))
                    }
                }
                ElementDefinitionObject::CompositeElementDefinition(element_definition) => {
                    let components: Vec<ElementComponent> = element_definition
                        .components
                        .iter()
                        .zip(element.components.iter())
                        .map(|(component_definition, component)| ElementComponent {
                            tag: component_definition.tag.to_string(),
                            description: component_definition.description.to_string(),
                            value: component.0.to_string(),
                        })
                        .collect();
                    if components.is_empty() { None} else {Some(Ok(ElementObject::CompositeElement(CompositeElement {
                        tag: element_definition.tag.to_string(),
                        name: element_definition.name.to_string(),
                        components,
                    })))}
                }
            };
            repetition
        })
        .collect();
    parsed
    /*
    let mut definition_iter = definition_elements.iter().peekable();

    let first_definition_element = definition_iter.peek().unwrap();

    let parsed = match first_definition_element {
        ElementDefinitionObject::ElementDefinition(element_definition) => Ok(vec![SegmentValue {
            elements: Repetition::One(Element {
                tag: element_definition.tag.to_string(),
            }),
        }]),
        ElementDefinitionObject::CompositeElementDefinition(element_definition) => {
            //definition_iter.zip(segment_elements.iter());
            Ok(vec![SegmentValue {
                elements: Repetition::One(Element {
                    tag: "any".to_string(),
                }),
            }])
        }
    };
    /*
    let mut definition_iter = definition_elements.iter();
    let definition = definition_iter.next().unwrap();
    let mut value_iter = segment_elements.iter();
    let value = value_iter.next();
    */
    parsed
    */
}

#[cfg(test)]
mod tests {
    use std::fs;
    use test_case::test_case;

    use super::*;

    #[test_case( "data/interchange/example_01.edi" ; "example 1")]
    #[test_case( "data/interchange/example_02.edi" ; "example 2")]
    //    #[ignore = "needs un directory, respect license"]
    fn test_read_interchange(filename: &str) {
        let input = fs::read_to_string(filename)
            .expect(format!("Error reading file {}", filename).as_str());

        let result = read_interchange(input.as_str());

        assert!(dbg!(result).is_ok())
    }

    #[test_case( "data/interchange/example_03.edi" ; "example 3 invalid")]
    //    #[ignore = "needs un directory, respect license"]
    fn test_read_interchange_invalid(filename: &str) {
        let input = fs::read_to_string(filename)
            .expect(format!("Error reading file {}", filename).as_str());

        let result = read_interchange(input.as_str());

        assert!(result.is_err())
    }
}
