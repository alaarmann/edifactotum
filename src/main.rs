use std::env;
use std::fs;
use std::process;

fn main() {
    let args: Vec<String> = env::args().collect();

    if args.len() != 2 {
        eprintln!("Usage: {} <EDIFACT-file>", args[0]);

        process::exit(1);
    }

    let filename = &args[1];

    let input =
        fs::read_to_string(filename).expect(format!("Error reading file {}", filename).as_str());

    let result = edifactotum::read_interchange(input.as_str());

    if let Err(e) = result {
        eprintln!("Parse error: {}", e);

        process::exit(1);
    }

    println!("{:#?}", result.unwrap());
}
