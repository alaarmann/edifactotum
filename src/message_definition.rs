use codepage_437::{CP437_CONTROL, BorrowFromCp437};
use pest::iterators::{Pair, Pairs};
use pest::Parser;
use serde::Serialize;
use std::error::Error;
use std::fs::read;
use crate::common::InclusionStatus;

#[derive(Parser)]
#[grammar = "message_definition.pest"]
pub struct MessageDefinitionParser;

#[derive(Debug, PartialEq, Serialize)]
pub struct MessageDefinition<'a> {
    pub objects: Vec<DefinitionObject<'a>>,
}

impl<'a> From<Pair<'a, Rule>> for MessageDefinition<'a> {
    fn from(pair: Pair<'a, Rule>) -> Self {
        let inner = pair.into_inner();
        let objects = inner
            .filter(|p| p.as_rule() == Rule::segment || p.as_rule() == Rule::segment_group)
            .map(DefinitionObject::from)
            .collect();

        MessageDefinition { objects }
    }
}

#[derive(Debug, PartialEq, Serialize)]
pub struct SegmentDefinition<'a> {
    pub position: &'a str,
    pub tag: &'a str,
    pub name: &'a str,
    pub status: InclusionStatus,
    pub repetition: usize,
}

impl<'a> From<Pair<'a, Rule>> for SegmentDefinition<'a> {
    fn from(pair: Pair<'a, Rule>) -> Self {
        let mut inner = pair.into_inner();

        let position = get_string_of_rule(&mut inner, Rule::position);
        let tag = get_string_of_rule(&mut inner, Rule::tag);
        let name = get_string_of_rule(&mut inner, Rule::name).trim_end();
        let status = InclusionStatus::from(get_string_of_rule(&mut inner, Rule::status));
        let repetition = get_string_of_rule(&mut inner, Rule::repetition)
            .parse()
            .unwrap();

        SegmentDefinition {
            position,
            tag,
            name,
            status,
            repetition,
        }
    }
}

#[derive(Debug, PartialEq, Serialize)]
pub struct SegmentGroupDefinition<'a> {
    pub position: &'a str,
    pub name: &'a str,
    pub status: InclusionStatus,
    pub repetition: usize,
    pub objects: Vec<DefinitionObject<'a>>,
}

impl<'a> From<Pair<'a, Rule>> for SegmentGroupDefinition<'a> {
    fn from(pair: Pair<'a, Rule>) -> Self {
        let mut inner = pair.into_inner();

        let position = get_string_of_rule(&mut inner, Rule::position);
        let name = get_string_of_rule(&mut inner, Rule::group_name).trim_end();
        let status = InclusionStatus::from(get_string_of_rule(&mut inner, Rule::status));
        let repetition = get_string_of_rule(&mut inner, Rule::repetition)
            .parse()
            .unwrap();
        let objects = inner
            .filter(|p| p.as_rule() == Rule::segment || p.as_rule() == Rule::segment_group)
            .map(DefinitionObject::from)
            .collect();

        SegmentGroupDefinition {
            position,
            name,
            status,
            repetition,
            objects,
        }
    }
}

fn get_string_of_rule<'a>(pairs: &mut Pairs<'a, Rule>, rule: Rule) -> &'a str {
    pairs
        .next()
        .filter(|p| p.as_rule() == rule)
        .unwrap()
        .as_str()
}

#[derive(Debug, PartialEq, Serialize)]
pub enum DefinitionObject<'a> {
    Segment(SegmentDefinition<'a>),
    SegmentGroup(SegmentGroupDefinition<'a>),
}

impl<'a> From<Pair<'a, Rule>> for DefinitionObject<'a> {
    fn from(pair: Pair<'a, Rule>) -> Self {
        match pair.as_rule() {
            Rule::segment => DefinitionObject::Segment(SegmentDefinition::from(pair)),
            Rule::segment_group => {
                DefinitionObject::SegmentGroup(SegmentGroupDefinition::from(pair))
            }
            _ => unreachable!(),
        }
    }
}

pub fn parse(input: &str) -> Result<MessageDefinition, Box<dyn Error>> {
    let parsed = MessageDefinitionParser::parse(Rule::message, input)?
        .next()
        .unwrap();

    let message_definition = MessageDefinition::from(parsed);
    Ok(message_definition)
}

pub fn to_json(message_definition: &MessageDefinition) -> String {
    serde_json::to_string(message_definition).unwrap()
}

pub fn read_file(filename: &str) -> Result<String, Box<dyn Error>> {
    let buffer = read(filename)?;
    Ok(String::borrow_from_cp437(&buffer, &CP437_CONTROL))
}

#[cfg(test)]
mod tests {
    use test_case::test_case;

    use super::*;

    #[test_case( "data/specification/message/EXAMPLE_D.20A" ; "simple")]
    #[test_case( "data/specification/message/EXAMPLE2_D.20A" ; "complex")]
    #[test_case( "data/specification/message/EXAMPLE_D.99B" ; "simple old")]
    #[test_case( "data/specification/message/EXAMPLE_D.95A" ; "simple very old")]
    #[ignore = "needs un directory, respect license"]
    fn parse_message_definition(filename: &str) {
        let input = read_file(filename)
            .expect(format!("Error reading file {}", filename).as_str());

        let result = parse(input.as_str());

        println!("{:?}", result);
        assert!(result.is_ok())
    }

    #[test_case( "00010   ABC Any name for segment                     M   2", SegmentDefinition {position: "00010", tag: "ABC", name: "Any name for segment", status: InclusionStatus::Mandatory, repetition: 2} ; "mandatory")]
    #[test_case( "00020   BCD Any name for BCD segment                 C   99", SegmentDefinition {position: "00020", tag: "BCD", name: "Any name for BCD segment", status: InclusionStatus::Conditional, repetition: 99} ; "conditional")]
    fn parse_segment_definition(input: &str, expected: SegmentDefinition) {
        let parsed = MessageDefinitionParser::parse(Rule::segment, input)
            .unwrap()
            .next()
            .unwrap();

        let result = SegmentDefinition::from(parsed);

        assert_eq!(result, expected)
    }

    #[test_case( "
00020       ---- Segment group 1  ------------------ C   99---------------+
00030   ABC A segment name for ABC                   M   1                |
00040   BCD A segment name for BCD                   C   99---------------+" , SegmentGroupDefinition {
    position: "00020",
    name: "Segment group 1",
    status: InclusionStatus::Conditional,
    repetition: 99,
    objects: vec![
        DefinitionObject::Segment(SegmentDefinition {
            position: "00030", 
            tag: "ABC", 
            name: "A segment name for ABC", 
            status: InclusionStatus::Mandatory,
            repetition: 1}),
        DefinitionObject::Segment(SegmentDefinition {
            position: "00040", 
            tag: "BCD", 
            name: "A segment name for BCD", 
            status: InclusionStatus::Conditional,
            repetition: 99}) ]
}; "simple")]
    #[test_case(
"                                                                          |
00200       ---- Segment group 3  ------------------ C   9---------------+|
00210   ABC A segment name for ABC                   M   1               ||
00220   BCD A segment name for BCD                   C   9---------------+"
, SegmentGroupDefinition {
    position: "00200",
    name: "Segment group 3",
    status: InclusionStatus::Conditional,
    repetition: 9,
    objects: vec![
        DefinitionObject::Segment(SegmentDefinition {
            position: "00210", 
            tag: "ABC", 
            name: "A segment name for ABC", 
            status: InclusionStatus::Mandatory, 
            repetition: 1}),
        DefinitionObject::Segment(SegmentDefinition {
            position: "00220", 
            tag: "BCD", 
            name: "A segment name for BCD", 
            status: InclusionStatus::Conditional, 
            repetition: 9}) ]
}; "contained")]
    #[test_case(
"
00160       ---- Segment group 2  ------------------ C   99999------------+
00170   ABC Name for ABC segment                     M   1                |
00180   BCD Name for BCD segment                     C   9                |
                                                                          |
00190       ---- Segment group 3  ------------------ C   9---------------+|
00200   CDE Name for CDE segment                     M   1               ||
00210   DEF Name for DEF segment                     C   9---------------++
"
, SegmentGroupDefinition {
    position: "00160",
    name: "Segment group 2",
    status: InclusionStatus::Conditional,
    repetition: 99999,
    objects: vec![
        DefinitionObject::Segment(SegmentDefinition {
            position: "00170", 
            tag: "ABC", 
            name: "Name for ABC segment", 
            status: InclusionStatus::Mandatory, 
            repetition: 1}),
        DefinitionObject::Segment(SegmentDefinition {
            position: "00180", 
            tag: "BCD", 
            name: "Name for BCD segment", 
            status: InclusionStatus::Conditional, 
            repetition: 9}),
        DefinitionObject::SegmentGroup(SegmentGroupDefinition {
            position: "00190",
            name: "Segment group 3",
            status: InclusionStatus::Conditional,
            repetition: 9,
            objects: vec![
                DefinitionObject::Segment(SegmentDefinition {
                    position: "00200", 
                    tag: "CDE", 
                    name: "Name for CDE segment", 
                    status: InclusionStatus::Mandatory, 
                    repetition: 1}),
                DefinitionObject::Segment(SegmentDefinition {
                    position: "00210", 
                    tag: "DEF", 
                    name: "Name for DEF segment", 
                    status: InclusionStatus::Conditional, 
                    repetition: 9}),
            ]
        })
    ]
}; "nested")]
    #[test_case(
"
00170       ---- Segment group 3  ------------------ C   99---------------+
00180   ABC Name for ABC segment                     M   1                |
00190   BCD Name for BCD segment                     C   6                |
                                                                          |
00200       ---- Segment group 4  ------------------ C   9---------------+|
00210   CDE Name for CDE segment                     M   1               ||
00220   DEF Name for DEF segment                     C   7---------------+|
                                                                          |
00230       ---- Segment group 5  ------------------ C   99--------------+|
00240   EFG Name for EFG segment                     M   1               ||
00250   FGH Name for FGH segment                     C   8---------------+|
                                                                          |
00260       ---- Segment group 6  ------------------ C   9---------------+|
00270   GHI Name for GHI segment                     M   1               ||
00280   HIJ Name for HIJ segment                     C   9---------------++
"
, SegmentGroupDefinition {
    position: "00170",
    name: "Segment group 3",
    status: InclusionStatus::Conditional,
    repetition: 99,
    objects: vec![
        DefinitionObject::Segment(SegmentDefinition {
            position: "00180", 
            tag: "ABC", 
            name: "Name for ABC segment", 
            status: InclusionStatus::Mandatory, 
            repetition: 1}),
        DefinitionObject::Segment(SegmentDefinition {
            position: "00190", 
            tag: "BCD", 
            name: "Name for BCD segment", 
            status: InclusionStatus::Conditional, 
            repetition: 6}),
        DefinitionObject::SegmentGroup(SegmentGroupDefinition {
            position: "00200",
            name: "Segment group 4",
            status: InclusionStatus::Conditional,
            repetition: 9,
            objects: vec![
                DefinitionObject::Segment(SegmentDefinition {
                    position: "00210", 
                    tag: "CDE", 
                    name: "Name for CDE segment", 
                    status: InclusionStatus::Mandatory, 
                    repetition: 1}),
                DefinitionObject::Segment(SegmentDefinition {
                    position: "00220", 
                    tag: "DEF", 
                    name: "Name for DEF segment", 
                    status: InclusionStatus::Conditional, 
                    repetition: 7}),
            ]
        }),
        DefinitionObject::SegmentGroup(SegmentGroupDefinition {
            position: "00230",
            name: "Segment group 5",
            status: InclusionStatus::Conditional,
            repetition: 99,
            objects: vec![
                DefinitionObject::Segment(SegmentDefinition {
                    position: "00240", 
                    tag: "EFG", 
                    name: "Name for EFG segment", 
                    status: InclusionStatus::Mandatory, 
                    repetition: 1}),
                DefinitionObject::Segment(SegmentDefinition {
                    position: "00250", 
                    tag: "FGH", 
                    name: "Name for FGH segment", 
                    status: InclusionStatus::Conditional, 
                    repetition: 8}),
            ]
        }),
        DefinitionObject::SegmentGroup(SegmentGroupDefinition {
            position: "00260",
            name: "Segment group 6",
            status: InclusionStatus::Conditional,
            repetition: 9,
            objects: vec![
                DefinitionObject::Segment(SegmentDefinition {
                    position: "00270", 
                    tag: "GHI", 
                    name: "Name for GHI segment", 
                    status: InclusionStatus::Mandatory, 
                    repetition: 1}),
                DefinitionObject::Segment(SegmentDefinition {
                    position: "00280", 
                    tag: "HIJ", 
                    name: "Name for HIJ segment", 
                    status: InclusionStatus::Conditional, 
                    repetition: 9}),
            ]
        })
    ]
}; "nested_multiple")]
    fn parse_segment_group_definition(input: &str, expected: SegmentGroupDefinition) {
        let parsed = MessageDefinitionParser::parse(Rule::segment_group, input)
            .unwrap()
            .next()
            .unwrap();

        let result = SegmentGroupDefinition::from(parsed);

        assert_eq!(result, expected)
    }

    #[test_case(MessageDefinition{
        objects: vec![DefinitionObject::SegmentGroup(SegmentGroupDefinition {
            position: "00020",
            name: "Segment group 1",
            status: InclusionStatus::Conditional,
            repetition: 99,
            objects: vec![
                DefinitionObject::Segment(SegmentDefinition {
                    position: "00030", 
                    tag: "ABC", 
                    name: "A segment name for ABC", 
                    status: InclusionStatus::Mandatory, 
                    repetition: 1}),
                DefinitionObject::Segment(SegmentDefinition {
                    position: "00040", 
                    tag: "BCD", 
                    name: "A segment name for BCD", 
                    status: InclusionStatus::Conditional, 
                    repetition: 99}) ]
        })] }, "{\"objects\":[{\"SegmentGroup\":{\"position\":\"00020\",\"name\":\"Segment group 1\",\"status\":\"Conditional\",\"repetition\":99,\"objects\":[{\"Segment\":{\"position\":\"00030\",\"tag\":\"ABC\",\"name\":\"A segment name for ABC\",\"status\":\"Mandatory\",\"repetition\":1}},{\"Segment\":{\"position\":\"00040\",\"tag\":\"BCD\",\"name\":\"A segment name for BCD\",\"status\":\"Conditional\",\"repetition\":99}}]}}]}"; "segment group")]
    fn serialize_to_json(input: MessageDefinition, expected: &str) {
        let result = serde_json::to_string(&input).unwrap();
        println!("{}", result);
        assert_eq!(result, expected)
    }

    #[test_case("data/specification/message/EXAMPLE_D.99B"; "directory d99b")]
    #[test_case("data/specification/message/EXAMPLE_D.20A"; "directory d20a")]
    #[ignore = "needs un directory, respect license"]
    fn read_file_cp437(input: &str) {
        let result = read_file(input);
        println!("{:?}", result);
        assert!(result.is_ok())
    }
}
