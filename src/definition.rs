use crate::common::InclusionStatus;
use crate::message_definition::{
    parse as parse_message_definition, DefinitionObject as RawDefinitionObject,
    MessageDefinition as RawMessageDefinition,
};
use crate::segment_definition::{
    parse as parse_segment_definition, ComponentDefinition as RawComponentDefinition,
    CompositeElementDefinition as RawCompositeElementDefinition,
    ElementDefinition as RawElementDefinition,
    ElementDefinitionObject as RawElementDefinitionObject,
    SegmentDefinition as RawSegmentDefinition,
};
use codepage_437::{BorrowFromCp437, CP437_CONTROL};
use serde::Serialize;
use std::error::Error;
use std::fs::read;
use std::path::{Path, PathBuf};

#[derive(Debug)]
pub struct MessageIdentifier<'a> {
    pub message_type: &'a str,
    pub version_number: &'a str,
    pub release_number: &'a str,
    pub controlling_agency: &'a str,
}

pub fn get_definition_for(
    base_dir: Box<Path>,
    message_identifier: &MessageIdentifier,
) -> Result<MessageDefinition, Box<dyn Error>> {
    let mut message_definition_file = base_dir.clone().into_path_buf();
    message_definition_file.push(make_path_of_message_definition_file(message_identifier));
    let message_definition_content = read_file(&message_definition_file)?;
    let message_definition = parse_message_definition(&message_definition_content)?;

    let mut segment_definition_file = base_dir.clone().into_path_buf();
    segment_definition_file.push(make_path_of_segment_definition_file(message_identifier));
    let segment_definition_content = read_file(&segment_definition_file)?;
    let segment_definitions = parse_segment_definition(segment_definition_content.as_str())?;
    augment_message_definition(&segment_definitions, &message_definition)
}

#[derive(Debug, PartialEq, Serialize)]
pub struct MessageDefinition {
    pub objects: Vec<DefinitionObject>,
}

#[derive(Debug, PartialEq, Serialize)]
pub struct SegmentDefinition {
    pub position: String,
    pub tag: String,
    pub name: String,
    pub status: InclusionStatus,
    pub repetition: usize,
    pub general_name: String,
    pub description: String,
    pub elements: Vec<ElementDefinitionObject>,
    pub note: Option<String>,
}

#[derive(Debug, PartialEq, Serialize)]
pub enum ElementDefinitionObject {
    ElementDefinition(ElementDefinition),
    CompositeElementDefinition(CompositeElementDefinition),
}

impl<'a> From<&RawElementDefinitionObject<'a>> for ElementDefinitionObject {
    fn from(raw: &RawElementDefinitionObject) -> Self {
        match raw {
            RawElementDefinitionObject::ElementDefinition(element_definition) => {
                ElementDefinitionObject::ElementDefinition(ElementDefinition::from(
                    element_definition,
                ))
            }
            RawElementDefinitionObject::CompositeElementDefinition(
                composite_element_definition,
            ) => ElementDefinitionObject::CompositeElementDefinition(
                CompositeElementDefinition::from(composite_element_definition),
            ),
        }
    }
}

#[derive(Debug, PartialEq, Serialize)]
pub struct ElementDefinition {
    position: String,
    pub tag: String,
    pub name: String,
    status: InclusionStatus,
    pub repetition: usize,
    data_type: String,
}

impl<'a> From<&RawElementDefinition<'a>> for ElementDefinition {
    fn from(raw: &RawElementDefinition) -> Self {
        ElementDefinition {
            position: raw.position.to_string(),
            tag: raw.tag.to_string(),
            name: raw.name.to_string(),
            status: raw.status,
            repetition: raw.repetition,
            data_type: raw.data_type.to_string(),
        }
    }
}

#[derive(Debug, PartialEq, Serialize)]
pub struct CompositeElementDefinition {
    position: String,
    pub tag: String,
    pub name: String,
    status: InclusionStatus,
    pub repetition: usize,
    pub components: Vec<ComponentDefinition>,
}

impl<'a> From<&RawCompositeElementDefinition<'a>> for CompositeElementDefinition {
    fn from(raw: &RawCompositeElementDefinition) -> Self {
        CompositeElementDefinition {
            position: raw.position.to_string(),
            tag: raw.tag.to_string(),
            name: raw.name.to_string(),
            status: raw.status,
            repetition: raw.repetition,
            components: raw
                .components
                .iter()
                .map(ComponentDefinition::from)
                .collect(),
        }
    }
}

#[derive(Debug, PartialEq, Serialize)]
pub struct ComponentDefinition {
    pub tag: String,
    pub description: String,
    status: InclusionStatus,
    data_type: String,
}

impl<'a> From<&RawComponentDefinition<'a>> for ComponentDefinition {
    fn from(raw: &RawComponentDefinition) -> Self {
        ComponentDefinition {
            tag: raw.tag.to_string(),
            description: raw.description.to_string(),
            status: raw.status,
            data_type: raw.data_type.to_string(),
        }
    }
}

#[derive(Debug, PartialEq, Serialize)]
pub struct SegmentGroupDefinition {
    pub position: String,
    pub name: String,
    pub status: InclusionStatus,
    pub repetition: usize,
    pub objects: Vec<DefinitionObject>,
}

#[derive(Debug, PartialEq, Serialize)]
pub enum DefinitionObject {
    Segment(SegmentDefinition),
    SegmentGroup(SegmentGroupDefinition),
}

fn augment_message_definition(
    segment_definitions: &Vec<RawSegmentDefinition>,
    message_definition: &RawMessageDefinition,
) -> Result<MessageDefinition, Box<dyn Error>> {
    let objects = augment_objects(segment_definitions, &message_definition.objects)?;
    Ok(MessageDefinition { objects })
}

fn augment_objects(
    segment_definitions: &Vec<RawSegmentDefinition>,
    objects: &Vec<RawDefinitionObject>,
) -> Result<Vec<DefinitionObject>, Box<dyn Error>> {
    objects
        .into_iter()
        .filter(|obj| {
            if let RawDefinitionObject::Segment(segment_definition) = obj {
                !segment_definition.tag.starts_with("UN")
            } else {
                true
            }
        })
        .try_fold(vec![], |mut acc, obj| {
            let augmented_object = augment_object(segment_definitions, obj)?;
            acc.push(augmented_object);
            Ok(acc)
        })
}

fn augment_object(
    segment_definitions: &Vec<RawSegmentDefinition>,
    object: &RawDefinitionObject,
) -> Result<DefinitionObject, Box<dyn Error>> {
    match object {
        RawDefinitionObject::Segment(segment_definition) => {
            if let Some(general_segment_definition) = segment_definitions
                .into_iter()
                .find(|&sd| sd.tag == segment_definition.tag)
            {
                Ok(DefinitionObject::Segment(SegmentDefinition {
                    name: segment_definition.name.to_string(),
                    position: segment_definition.position.to_string(),
                    tag: segment_definition.tag.to_string(),
                    repetition: segment_definition.repetition,
                    status: segment_definition.status,
                    general_name: general_segment_definition.name.to_string(),
                    description: general_segment_definition.description.to_string(),
                    elements: general_segment_definition
                        .elements
                        .iter()
                        .map(ElementDefinitionObject::from)
                        .collect(),
                    note: general_segment_definition.note.as_ref().map(String::clone),
                }))
            } else {
                let err: Box<dyn Error + Send + Sync> = From::from(format!(
                    "No segment definition found for tag {}",
                    segment_definition.tag
                ));
                Err(err as Box<dyn Error>)
            }
        }
        RawDefinitionObject::SegmentGroup(segment_group_definition) => {
            let objects = augment_objects(segment_definitions, &segment_group_definition.objects)?;
            Ok(DefinitionObject::SegmentGroup(SegmentGroupDefinition {
                name: segment_group_definition.name.to_string(),
                position: segment_group_definition.position.to_string(),
                status: segment_group_definition.status,
                repetition: segment_group_definition.repetition,
                objects,
            }))
        }
    }
}

fn make_path_of_message_definition_file(message_identifier: &MessageIdentifier) -> PathBuf {
    let mut path = make_path_of_definition_dir(message_identifier);
    path.push(
        format!(
            "{}_{}.{}",
            message_identifier.message_type,
            message_identifier.version_number,
            message_identifier.release_number
        )
        .to_uppercase(),
    );
    path
}
fn make_path_of_segment_definition_file(message_identifier: &MessageIdentifier) -> PathBuf {
    let mut path = make_path_of_definition_dir(message_identifier);
    path.push(
        format!(
            "{}SD.{}",
            message_identifier.version_number, message_identifier.release_number
        )
        .to_uppercase(),
    );
    path
}
fn make_path_of_definition_dir(message_identifier: &MessageIdentifier) -> PathBuf {
    let mut path = PathBuf::from(message_identifier.controlling_agency.to_lowercase());
    path.push(
        format!(
            "{}{}",
            message_identifier.version_number, message_identifier.release_number
        )
        .to_lowercase(),
    );
    path
}

pub fn read_file(filename: &Path) -> Result<String, Box<dyn Error>> {
    let buffer = read(filename)?;
    Ok(String::borrow_from_cp437(&buffer, &CP437_CONTROL))
}

#[cfg(test)]
mod tests {
    use test_case::test_case;

    use super::*;

    #[test_case( MessageIdentifier{
    message_type: "EXAMPLE",
    version_number: "D",
    release_number: "20A",
    controlling_agency: "XY"

    }; "example")]
    #[ignore = "needs un directory, respect license"]
    fn test_get_definition_for(message_identifier: MessageIdentifier) {
        let result = get_definition_for(
            PathBuf::from("data/specification").into_boxed_path(),
            &message_identifier,
        );
        println!("{:?}", result);
        assert!(result.is_ok())
    }

    #[test_case( MessageIdentifier{
    message_type: "EXAMPLE",
    version_number: "D",
    release_number: "20A",
    controlling_agency: "XY"

    }; "example")]
    fn test_make_path_of_message_definition_file(message_identifier: MessageIdentifier) {
        let result = make_path_of_message_definition_file(&message_identifier);

        assert_eq!(result, PathBuf::from("xy/d20a/EXAMPLE_D.20A"))
    }

    #[test_case( MessageIdentifier{
    message_type: "EXAMPLE",
    version_number: "D",
    release_number: "20A",
    controlling_agency: "XY"

    }; "example")]
    fn test_make_path_of_segment_definition_file(message_identifier: MessageIdentifier) {
        let result = make_path_of_segment_definition_file(&message_identifier);

        assert_eq!(result, PathBuf::from("xy/d20a/DSD.20A"))
    }
}
